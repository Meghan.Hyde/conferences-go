from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    pic_url = "https://api.pexels.com/v1/search"
    pic_params = {
        "query": f"{city}, {state}",
        "per_page": 1
    }
    response = requests.get(pic_url, params=pic_params, headers=headers)
    content = json.loads(response.content)
    # Make the request
    # Parse the JSON response

    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}



def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"

    geocode_params = {
        "q": f"{city},{state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }

    # Make the request
    geocode_response = requests.get(geocode_url, params=geocode_params)
    # geocode_response = geocode_response.json()
    content = json.loads(geocode_response.content)
    # Parse the JSON response
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None


    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # Create the URL for the current weather API with the latitude
    #   and longitude

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }


    # Make the request
    weather_response = requests.get(weather_url, params=weather_params)
    # weather_response = weather_response.json()
    content = json.loads(weather_response.content)
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"]
        }
    except (KeyError, IndexError):
        return None
    # Return the dictionary

